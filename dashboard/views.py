from django.template import RequestContext
from django.shortcuts import render_to_response
from django.conf import settings
from django.http import HttpResponse, JsonResponse
from django.core.urlresolvers import reverse
from django.views.decorators.http import require_http_methods
from django.utils.translation import ugettext as _

from decorators import request_is_ajax
from .forms import IndexesForm
from .mappers import grid_records_mapper, grid_calculated_mapper, fixed_costs_mapper

from accountsmgm.models import Accounts
from lib.records import RecordsManager
from lib.costs import CostsManager
from lib.estimator import IndexEstimator


def main(request):
    """Main page for dashboard"""
    form = IndexesForm
    form.post_url = reverse('dashboard:save_indexes')

    cm = CostsManager()
    electricity_fixed_costs, gas_fixed_costs, total_fixed_costs = cm.get_all_fixed_costs(mapper_func=fixed_costs_mapper)

    template_data = {
        'logged_code': request.session.get(settings.SESSION_ACCOUNT_CODE),
        'form': form,
        'electricity_fixed_costs': electricity_fixed_costs,
        'gas_fixed_costs': gas_fixed_costs,
        'total_fixed_costs': total_fixed_costs
    }

    return render_to_response('dashboard/main.html', template_data, context_instance=RequestContext(request))


@request_is_ajax
@require_http_methods(["POST"])
def save_indexes(request):
    # current_account_code = request.session.get(settings.SESSION_ACCOUNT_CODE, False)
    # current_account = Accounts.objects.filter(unique_code=current_account_code).first()
    account_id = request.session.get(settings.SESSION_ACCOUNT_ID, 0)

    form = IndexesForm(request.POST)

    try:
        if form.is_valid():
            clean_data = form.cleaned_data

            rm = RecordsManager(account_id=account_id, energy_type=clean_data['energy_type'])
            rm.index_estimator = IndexEstimator
            rm.costs_manager = CostsManager

            existing_record = rm.get_daily_record(clean_data['date_recorded'])

            if existing_record:
                form = IndexesForm(request.POST, instance=existing_record)

            # inserting the missing account field
            f = form.save(commit=False)
            f.account = Accounts.objects.get(pk=account_id)
            f.save()

            # now do post calculation for previous months
            rm.do_previous_months_calculation(clean_data)

            result = {'result': 1, 'msg': _('Data was successfully saved.')}
        else:
            print(form.errors) # debug only
            result = {'result': 0, 'msg': _('An error occurred. Please try again.')}
    except Exception as e:
        result = {'result': 0, 'msg': e.__str__()}

    return JsonResponse(result)


@request_is_ajax
@require_http_methods(["GET"])
def get_indexes(request):
    account_id = request.session.get(settings.SESSION_ACCOUNT_ID, 0)

    rm = RecordsManager(account_id=account_id)
    result = rm.get_all_records(grid_records_mapper)

    return JsonResponse(result, safe=False)


@request_is_ajax
@require_http_methods(["GET"])
def get_calculated_costs(request):
    account_id = request.session.get(settings.SESSION_ACCOUNT_ID, 0)

    cm = CostsManager(account_id=account_id)
    result = cm.get_all_calculated_costs(grid_calculated_mapper)

    return JsonResponse(result, safe=False)
