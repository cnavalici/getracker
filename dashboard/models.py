"""
python manage.py makemigrations --dry-run dashboard
python manage.py sqlmigrate dashboard <name_of_the_migration_file>
python manage.py migrate
"""

from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext as _
from accountsmgm import models as account_models
from django.forms.models import model_to_dict

ELECTRICITY_CODE = 1
GAS_CODE = 2

ENERGY_TYPES = (
    (ELECTRICITY_CODE, _('electricity')),
    (GAS_CODE, _('gas'))
)


class AccountQuerySet(models.QuerySet):
    def account(self, account_id):
        return self.filter(account_id=account_id)


class GETBaseModel(models.Model):
    objects = models.Manager()
    filtered = AccountQuerySet.as_manager()

    class Meta:
        abstract = True


class TariffsMonthly(models.Model):
    name = models.CharField(max_length=64)
    value = models.DecimalField(max_digits=8, decimal_places=2)
    energy_type = models.PositiveSmallIntegerField(choices=ENERGY_TYPES, default=1)
    start_date = models.DateField()
    end_date = models.DateField()

    def __str__(self):
        return "%s - %s %s (%s - %s)" % (self.energy_type, self.name, self.value, self.start_date, self.end_date)


class TariffsVariable(models.Model):
    value = models.DecimalField(max_digits=8, decimal_places=2)
    energy_type = models.PositiveSmallIntegerField(choices=ENERGY_TYPES, default=1)
    start_date = models.DateField()
    end_date = models.DateField()

    def __str__(self):
        return "%s - %s (%s - %s)" % (self.energy_type, self.value, self.start_date, self.end_date)


class Records(GETBaseModel):
    account = models.ForeignKey(account_models.Accounts)
    index_value = models.PositiveIntegerField()
    date_recorded = models.DateField(default=timezone.now)
    energy_type = models.PositiveSmallIntegerField(choices=ENERGY_TYPES, default=1)

    @staticmethod
    def get_statuses():
        return dict(ENERGY_TYPES)

    def __str__(self):
        statuses = self.get_statuses()
        recorded = self.date_recorded.strftime("%Y-%m-%d")
        return "[%s][%s][%s]: %s" % (self.account.unique_code, statuses[self.energy_type], recorded, self.index_value)


class CalculatedCosts(GETBaseModel):
    account = models.ForeignKey(account_models.Accounts)
    date_recorded = models.DateField()
    first_index = models.PositiveIntegerField()
    last_index = models.PositiveIntegerField(default=0)
    costs_variable = models.DecimalField(max_digits=8, decimal_places=2)
    costs_fixed = models.DecimalField(max_digits=8, decimal_places=2)
    energy_type = models.PositiveSmallIntegerField(choices=ENERGY_TYPES, default=1)

    def __str__(self):
        return "%s:%s (%s - %s)" % (self.energy_type, self.date_recorded, self.first_index, self.last_index)
