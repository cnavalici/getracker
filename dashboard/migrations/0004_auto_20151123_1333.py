# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    def load_tariffs(apps, schema_editor):
        # We get the model from the versioned app registry;
        # if we directly import it, it'll be the wrong version
        db_alias = schema_editor.connection.alias

        TariffsMonthly = apps.get_model("dashboard", "TariffsMonthly")
        TariffsMonthly.objects.using(db_alias).all().delete()
        TariffsMonthly.objects.using(db_alias).bulk_create([
            TariffsMonthly(energy_type=1, name="Vaste leveringkosten", value=3.75, end_date="2016-07-16", start_date="2015-01-01"),
            TariffsMonthly(energy_type=2, name="Vaste leveringkosten", value=3.75, end_date="2016-07-16", start_date="2015-01-01"),
            TariffsMonthly(energy_type=1, name="Netbeheerkosten", value=13.40, end_date="2016-07-16", start_date="2015-01-01"),
            TariffsMonthly(energy_type=2, name="Netbeheerkosten", value=20.01, end_date="2016-07-16", start_date="2015-01-01"),
            TariffsMonthly(energy_type=2, name="Energie belasting", value=-31.44, end_date="2016-07-16", start_date="2015-01-01")
        ])

        TariffsVariable = apps.get_model("dashboard", "TariffsVariable")
        TariffsVariable.objects.using(db_alias).all().delete()
        TariffsVariable.objects.using(db_alias).bulk_create([
            TariffsVariable(energy_type=1, value=0.22, end_date="2016-07-16", start_date="2015-01-01"),
            TariffsVariable(energy_type=2, value=0.53, end_date="2016-07-16", start_date="2015-01-01")
        ])

    dependencies = [
        ('dashboard', '0003_auto_20151109_1057'),
    ]

    operations = [
        migrations.RunPython(load_tariffs)
    ]
