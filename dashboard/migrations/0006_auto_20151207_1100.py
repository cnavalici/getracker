# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0005_calculatedcosts'),
    ]

    operations = [
        migrations.AlterField(
            model_name='calculatedcosts',
            name='last_index',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
