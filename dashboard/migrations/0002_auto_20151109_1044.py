# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='records',
            name='date_recorded',
            field=models.DateField(default=datetime.datetime(2015, 11, 9, 10, 44, 47, 942414, tzinfo=utc)),
        ),
    ]
