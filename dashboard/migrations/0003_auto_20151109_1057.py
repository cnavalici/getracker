# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0002_auto_20151109_1044'),
    ]

    operations = [
        migrations.AlterField(
            model_name='records',
            name='date_recorded',
            field=models.DateField(default=django.utils.timezone.now),
        ),
    ]
