# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accountsmgm', '0003_auto_20151105_0919'),
        ('dashboard', '0004_auto_20151123_1333'),
    ]

    operations = [
        migrations.CreateModel(
            name='CalculatedCosts',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('date_recorded', models.DateField()),
                ('first_index', models.PositiveIntegerField()),
                ('last_index', models.PositiveIntegerField()),
                ('costs_variable', models.DecimalField(decimal_places=2, max_digits=8)),
                ('costs_fixed', models.DecimalField(decimal_places=2, max_digits=8)),
                ('energy_type', models.PositiveSmallIntegerField(choices=[(1, 'electricity'), (2, 'gas')], default=1)),
                ('account', models.ForeignKey(to='accountsmgm.Accounts')),
            ],
        ),
    ]
