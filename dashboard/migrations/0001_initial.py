# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accountsmgm', '0003_auto_20151105_0919'),
    ]

    operations = [
        migrations.CreateModel(
            name='Records',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('index_value', models.PositiveIntegerField()),
                ('date_recorded', models.DateTimeField(auto_now=True)),
                ('energy_type', models.PositiveSmallIntegerField(default=1, choices=[(1, 'electricity'), (2, 'gas')])),
                ('account', models.ForeignKey(to='accountsmgm.Accounts')),
            ],
        ),
        migrations.CreateModel(
            name='TariffsMonthly',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('name', models.CharField(max_length=64)),
                ('value', models.DecimalField(decimal_places=2, max_digits=8)),
                ('energy_type', models.PositiveSmallIntegerField(default=1, choices=[(1, 'electricity'), (2, 'gas')])),
                ('start_date', models.DateField()),
                ('end_date', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='TariffsVariable',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('value', models.DecimalField(decimal_places=2, max_digits=8)),
                ('energy_type', models.PositiveSmallIntegerField(default=1, choices=[(1, 'electricity'), (2, 'gas')])),
                ('start_date', models.DateField()),
                ('end_date', models.DateField()),
            ],
        ),
    ]
