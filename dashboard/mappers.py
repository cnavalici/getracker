"""
This file contains mappers - functions that will transform raw server side data
into UI prefered formats

similar to serializers but simpler and more specific
"""

from django.forms.models import model_to_dict
from .models import Records, ELECTRICITY_CODE, GAS_CODE


def grid_records_mapper(raw_data):
    """
    mapper for records grid
    """
    energy_types = Records.get_statuses()

    result = []
    for rd in raw_data:
        row = model_to_dict(rd)
        row['energy_type'] = energy_types[rd.energy_type]
        result.append(row)

    return result


def indexes_to_estimator_mapper(raw_data):
    """
    mapper to tranform all the month records into estimator format
    { 'date_as_string': index_value ... }
    """
    result = {}
    for rd in raw_data:
        date_record = rd.date_recorded.strftime('%Y-%m-%d')
        result[date_record] = rd.index_value

    return result


def grid_calculated_mapper(raw_data):
    """
    mapper for calculated costs grid
    """
    energy_types = Records.get_statuses()

    result = []
    for rd in raw_data:
        row = model_to_dict(rd)
        row['energy_type'] = energy_types[rd.energy_type]
        row['diff_index'] = row["last_index"] - row["first_index"]
        result.append(row)

    return result


def fixed_costs_mapper(raw_data):
    """
    mapper for the fixed (monthly) costs
    :param raw_data:
    :return:
    """
    energy_types = Records.get_statuses()

    result_e = []
    result_g = []
    total_e = total_g = 0

    for rd in raw_data:
        result_row = {'name': rd.name, 'value': rd.value}

        if rd.energy_type == ELECTRICITY_CODE:
            result_e.append(result_row)
            total_e = total_e + rd.value

        if rd.energy_type == GAS_CODE:
            result_g.append(result_row)
            total_g = total_g + rd.value

    totals = {energy_types[ELECTRICITY_CODE]: total_e, energy_types[GAS_CODE]: total_g, 'bigtotal':total_g + total_e}

    return result_e, result_g, totals
