from django import forms
from django.utils.translation import ugettext as _
from django.utils.timezone import now, utc
from datetime import timedelta, date, datetime

from .models import Records, ENERGY_TYPES


class IndexesForm(forms.ModelForm):
    """ this form is used to enter new indexes (records) into the system """
    TODAY_DATE = now()
    # TODAY_DATE = datetime.strptime('2015-01-01', '%Y-%m-%d')
    TODAY_DATE_ISO = date.isoformat(TODAY_DATE)

    MAX_DATE = TODAY_DATE + timedelta(days=365)  # don't bother about leap years here
    MAX_DATE_ISO = date.isoformat(MAX_DATE)

    INDEX_MIN = 1
    INDEX_MAX = 1000000

    # this is populated from the view
    post_url = ''

    energy_type = forms.ChoiceField(
        label=_("Type"), required=True,
        choices=ENERGY_TYPES,
        widget=forms.Select(attrs={
            'style': "width: 100px;",
            'required': 'required',
            'data-dojo-type': 'dijit/form/Select',
            'data-dojo-props': "invalidMessage: 'Index Value Required'"
        })
    )

    index_range_msg = _("Provide a value between %(min)s and %(max)s") % {'min': INDEX_MIN, 'max': INDEX_MAX}
    index_min_max = "{{min:{0},max:{1},places:0}}".format(INDEX_MIN, INDEX_MAX)

    index_value = forms.IntegerField(
        label=_("Index Value"), required=True,
        max_value=INDEX_MAX, min_value=INDEX_MIN,
        widget=forms.TextInput(attrs={
            'trim': 'trim',
            'required': 'required',
            'data-dojo-type': 'dijit/form/NumberTextBox',
            'data-dojo-props': "invalidMessage: '{0}', rangeMessage:'{1}', constraints: {2}"
                .format(_('Numeric value only'), index_range_msg, index_min_max)
        })
    )

    date_range_msg = _("Date range is %(min)s to %(max)s" % {'min': TODAY_DATE_ISO, 'max': MAX_DATE_ISO}),
    date_min_max = "{{min: '{0}', max: '{1}' }}".format(TODAY_DATE_ISO, MAX_DATE_ISO)

    date_recorded = forms.DateField(
        label=_("Records date"),
        widget=forms.TextInput(attrs={
            'id': 'scheduled_at_date',
            'data-dojo-type': 'dijit/form/DateTextBox',
            'data-dojo-props': "constraints: {0}".format(date_min_max),
            'rangeMessage': date_range_msg,
            'value': TODAY_DATE_ISO
        }),
        required=True,
        input_formats=['%Y-%m-%d']
    )

    def clean(self):
        cleaned_data = super(IndexesForm, self).clean()
        form_date_recorded = cleaned_data.get('date_recorded')

        if not form_date_recorded or \
                        form_date_recorded < self.TODAY_DATE.date() or form_date_recorded > self.MAX_DATE.date():
             raise forms.ValidationError("The date is not in the allowed range.")

    class Meta:
        model = Records
        exclude = ['account']