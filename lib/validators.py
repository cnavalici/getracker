from django.core.exceptions import ValidationError
from uuid import UUID


def validate_uuid4(uuid_string):
    try:
        return UUID(uuid_string, version=4)
    except ValueError:
        raise ValidationError('%s is not a valid UUID4' % uuid_string)
