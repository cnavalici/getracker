import uuid
import logging

from accountsmgm.models import Accounts
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.conf import settings
from django.utils.translation import ugettext as _


class AccountsManager(object):
    def __init__(self):
        self.logger = logging.getLogger(settings.GENERAL_LOG_NAME)

    def create_new_account(self, target_email, send_email=True):
        new_code = str(uuid.uuid4())

        new_account = Accounts()
        new_account.email_address = target_email
        new_account.unique_code = new_code
        new_account.save()

        if send_email:
            msg_content = render_to_string('emails/new_account.html', { 'access_code': new_code })
            msg = EmailMessage(_("GETracker - New account created for you"), msg_content,
                               settings.EMAIL_DEFAULT_SENDER, [target_email])
            msg.content_subtype = "html"
            msg.send()

            self.logger.info("Sent new account email to: {}".format(target_email))

        return new_code

    def recover_account(self, associated_email):
        """
        sends a message with all associated codes related to an email address
        :returns number of existing accounts for the specific email address
        """
        accounts = Accounts.objects.filter(email_address=associated_email).all()

        if accounts:
            msg_content = render_to_string('emails/recover_account.html', {'accounts': accounts})

            msg = EmailMessage(_("GETracker - Recovering account's codes for you"), msg_content,
                               settings.EMAIL_DEFAULT_SENDER, [associated_email])
            msg.content_subtype = "html"
            msg.send()

            self.logger.info("Sent recover email to: {0} for {1} accounts.".format(associated_email, len(accounts)))

        return len(accounts)

