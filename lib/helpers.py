"""
Various helpers

"""
from datetime import datetime, timedelta

class DateHelper(object):
    date_format = '%Y-%m-%d'

    def __init__(self, as_object=True):
        # this will return replies as DateTime objects (where applicable)
        self.as_object = as_object

    def get_first_day_next_month(self, current_date):
        """
        given a current date object, return the first day in the next month
        e.g. for current_date (2015-02-12) return 2015-03-01
        """
        month = current_date.month
        year = current_date.year

        next_month = month + 1
        if next_month > 12:
            next_month = 1
            year = year + 1

        result = "{}-{:02}-01".format(year, next_month)

        if self.as_object:
            result = datetime.strptime(result, self.date_format).date()

        return result

    @staticmethod
    def generate_intervals(milestones):
        """
        given a list of datetime objects, generate intervals based on these values
        :returns tuple of intervals as (start, end)
        """
        intervals = []

        for counter in range(0, len(milestones)- 1):
            start_interval = milestones[counter]
            end_interval = milestones[counter+1]

            try:
                next_interval = milestones[counter+2]
            except IndexError:
                next_interval = milestones[counter+1]

            if (next_interval - end_interval).days > 1:
                end_interval = end_interval - timedelta(days=1)

            if start_interval != end_interval:
                # filter one day intervals
                intervals.append((start_interval, end_interval))

        return intervals

    @staticmethod
    def is_same_month(first_date, second_date):
        """
        compares two dates if they are in the same month (of the same year!)
        :param first_date: date object
        :param second_date: date object
        :return:
        """
        return (first_date.month == second_date.month) and (first_date.year == second_date.year)

    @staticmethod
    def difference_in_months(date_1, date_2):
        """
        calculates the difference in months between two dates (order of dates not important)
        Note: the difference is not including the last month, so between 2015-01 and 2015-03, diff is 2
                between 2015-01 and 2016-01 diff is 12, and so on...
        :param date_1: date object
        :param date_2: date object
        """
        if date_1 == date_2:
            return 0

        if date_1 > date_2:
            first = date_2
            second = date_1
        else:
            first = date_1
            second = date_2

        if second.year == first.year:
            months_diff = second.month - first.month
        else:
            months_diff = (second.year - first.year - 1) * 12 + (12 - first.month + second.month)

        return months_diff
