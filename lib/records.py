from datetime import datetime, date

from django.db.models import Q
from dashboard.models import Records, CalculatedCosts
from dashboard.mappers import indexes_to_estimator_mapper
from lib.helpers import DateHelper


class RecordsManager(object):
    # account id main filter
    account_id = 0

    # costs manager
    costs_manager = None

    # index estimator
    index_estimator = None

    def __init__(self, account_id, energy_type = None):
        if not account_id:
            raise Exception("No account id provided.")

        self.account_id = account_id
        self.energy_type = energy_type

    def get_daily_record(self, date_recorded):
        """
            a daily record means per energy type + per day you might have only one index
        """
        return Records.filtered.account(self.account_id)\
            .filter(date_recorded=date_recorded)\
            .filter(energy_type=self.energy_type)\
            .first()

    def get_all_records(self, mapper_func = None):
        """
        retrieves all recorded indexes (reverse ordered)
        """
        all_records = Records.filtered.account(self.account_id).all().order_by('-date_recorded')

        if mapper_func:
            all_records = mapper_func(all_records)

        return all_records

    def do_previous_months_calculation(self, input_data):
        """
        it calculates the previous months start/end indexes based on the new input
        and updates the costs
        """
        energy_type = input_data['energy_type']
        date_recorded = input_data['date_recorded']
        index_value = input_data['index_value']
        limit_data = {date_recorded.__str__(): index_value}

        # initialize cost manager
        costs_manager = self.costs_manager(account_id=self.account_id, energy_type=energy_type)

        current_month_date = date.today()
        # current_month_date = datetime.strptime('2015-06-12', '%Y-%m-%d').date()

        open_month_record = self.get_open_month()
        if not open_month_record:
            raise Exception("No open month found. Something must be wrong.")

        # if we have no records at all, append the first one
        if open_month_record == -1:
            costs_manager.insert_precalculated_costs(date_recorded, index_value)
            return 1

        # if we have only the current month, then nothing to do
        open_month_date = open_month_record.date_recorded
        if DateHelper.is_same_month(open_month_date, current_month_date):
            return 0

        # finally, we have some calculation to do
        dh = DateHelper()

        diff_months = DateHelper.difference_in_months(open_month_date, current_month_date)
        for index in range(1, diff_months + 1):
            if index == 1:
                # first month will be the one with records already saved
                records = self.get_open_month_records(open_month_date, indexes_to_estimator_mapper)
                next_month_date = open_month_date
            else:
                # those are intermediate months (no records)
                next_month_date = dh.get_first_day_next_month(next_month_date)
                records = {next_month_date.__str__(): previous_index}

            estimated_values, estimated_dates = self.__do_estimation(records, limit_data)

            previous_index = estimated_values[0]

            costs_manager.update_calculated_costs(estimated_dates[0], estimated_values[0])
            costs_manager.insert_precalculated_costs(estimated_dates[1], estimated_values[0])

            # print(index, previous_index, estimated_values, estimated_dates) # debug only

        return diff_months

    def get_open_month(self):
        """
        it gets the open month (not calculated yet)
        return the record for the open month, -1 if no records at all
        """
        if CalculatedCosts.filtered.account(self.account_id)\
                .filter(energy_type=self.energy_type)\
                .count() == 0:
            return -1

        first = CalculatedCosts.filtered.account(self.account_id)\
            .filter(energy_type=self.energy_type)\
            .filter(last_index=0)\
            .first()

        return first

    def get_open_month_records(self, target_date, mapper_func = None):
        """
        gets all the records within a specified month (by target date)
        :param target_date: date object defining target month
        """
        records = Records.filtered.account(self.account_id)\
            .filter(energy_type=self.energy_type)\
            .filter(date_recorded__month=target_date.month)\
            .filter(date_recorded__year=target_date.year)

        if mapper_func:
            records = mapper_func(records)

        return records

    def __do_estimation(self, records_data, limit_data):
        """
        common estimation method
        """
        es = self.index_estimator()
        es.populate(current_data=records_data, next_data=limit_data)

        estimated_values, estimated_dates = es.estimate()

        return [estimated_values, estimated_dates]