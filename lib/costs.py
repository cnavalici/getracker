"""
This manager is dealing with all related cost stuff

"""
from datetime import date

from dashboard.models import CalculatedCosts, TariffsMonthly, TariffsVariable

from lib.helpers import DateHelper


class CostsManager(object):
    account_id = 0
    monthly_tariffs_data = None
    variable_tariffs_data = None

    def __init__(self, account_id = 0, energy_type=None):
        self.energy_type = energy_type
        if energy_type:
            self.monthly_tariffs_data = self.__populate_monthly_tariffs(energy_type)
            self.variable_tariffs_data = self.__populate_variable_tariffs(energy_type)
        self.account_id = account_id

    def update_calculated_costs(self, month_end_date, month_end_index):
        month_record = CalculatedCosts.filtered.account(self.account_id)\
            .filter(date_recorded__month=month_end_date.month, date_recorded__year=month_end_date.year,
                    energy_type=self.energy_type)\
            .first()

        if not month_record:
            raise Exception("Calculated costs for month {} (energy: {}) not found."\
                            .format(month_end_date.month, self.energy_type))

        month_record.last_index = month_end_index

        # variable costs
        variable_tariff = self.get_variable_tariff(month_record.date_recorded)
        variable_costs = (month_record.last_index - month_record.first_index) * variable_tariff

        # fixed monthly costs
        fixed_costs = self.get_monthly_tariff(month_record.date_recorded)

        month_record.costs_variable = variable_costs
        month_record.costs_fixed = fixed_costs

        month_record.save()

    def get_variable_tariff(self, target_date):
        """
        gets the variable tariff active for a specific date
        the intervals data got pre-calculated in __populate_variable_tariffs
        :param target_date datetime object
        """
        return self.__search_in_tariffs(self.variable_tariffs_data, target_date)

    def get_monthly_tariff(self, target_date):
        """
        gets the monthly tariff active for a specific date
        the intervals data got pre-calculated in __populate_monthly_tariffs
        :param target_date datetime object
        """
        return self.__search_in_tariffs(self.monthly_tariffs_data, target_date)

    def get_all_calculated_costs(self, mapper_func = None):
        """
        gets already calculated costs
        """
        all_records = CalculatedCosts.filtered.account(self.account_id).exclude(last_index=0).order_by('-date_recorded')

        if mapper_func:
            all_records = mapper_func(all_records)

        return all_records

    def get_all_fixed_costs(self, target_date = date.today(), mapper_func = None):
        """
        gets the list of all monthly costs for a specific month
        :param target_date - date object
        """
        all_records = TariffsMonthly.objects\
            .filter(start_date__lte=target_date)\
            .filter(end_date__gte=target_date)

        if self.energy_type:
            all_records = all_records.filter(energy_type=self.energy_type)

        if mapper_func:
            all_records = mapper_func(all_records)

        return all_records

    def __search_in_tariffs(self, dataset, target_date):
        """
        common method to search for a date in a collection of intervals
        """
        result = 0
        for ds in dataset:
            if ds[0] <= target_date <= ds[1]:
                result = ds[2]
                break

        return result

    def __populate_variable_tariffs(self, energy_type):
        """
        this is a little bit simpler than monthly, we can have only one tariff active for an interval
        just arrange them in a nice manner
        """
        tariffs = TariffsVariable.objects.filter(energy_type=energy_type).order_by('start_date')

        intervals_tariffs = []
        for tariff in tariffs:
            intervals_tariffs.append((tariff.start_date, tariff.end_date, float(tariff.value)))

        return intervals_tariffs

    def __populate_monthly_tariffs(self, energy_type):
        """
        based on dates segments, we're gonna have overlapping values (those needs to create sums)
        check docs/monthly_tariffs for more detailed information
        """
        tariffs = TariffsMonthly.objects.filter(energy_type=energy_type).order_by('start_date')

        # extract first the milestones
        milestones = []
        for tariff in tariffs:
            milestones.append(tariff.start_date)
            milestones.append(tariff.end_date)

        # process only unique dates
        milestones = list(set(milestones))
        milestones.sort()

        # generate intervals based on our milestones
        intervals = DateHelper.generate_intervals(milestones)

        # at last, calculate the sum of the tariffs for each interval
        intervals_sum = []
        for start, end in intervals:
            sum = 0
            for tariff in tariffs:
                ts = tariff.start_date
                te = tariff.end_date

                if (ts < start and end < te) or ts == start or end == te:
                    sum = sum + tariff.value

            intervals_sum.append((start, end, float(sum)))

        return intervals_sum

    def insert_precalculated_costs(self, date_recorded, index_value):
        precalculated = CalculatedCosts(
            account_id=self.account_id, date_recorded=date_recorded, first_index=index_value,
            last_index=0, costs_variable=0, costs_fixed=0, energy_type=self.energy_type)
        precalculated.save()


