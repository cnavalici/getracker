"""
Indexes Estimator

used to estimate index values (for last day of the specified month + first day next month)
- for estimation the dataset should contain only dates from the same month
- the estimated value will be checked against provided limit (next_data)
- adjustments will be made if estimated > provided limit

"""
import re
import numpy
import calendar

from datetime import datetime, timedelta
from lib.helpers import DateHelper


class IndexEstimator(object):
    # simple regex check for dates
    date_pattern = re.compile("^(20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$")

    def __init__(self):
        self.current_data = None  # current month data
        self.next_data = None     # some next month data (used as a limit)

        self.estimated_values = []  # the final results values [last day estimation, next day estimation]
        self.estimated_dates = []  # the corresponding dates for the final result

        self.__last_day_current_month = None  # last day of the current month (date object)
        self.__first_day_next_month = None  # first day of the next month

    def populate(self, current_data, next_data):
        """
        Runs some consistency checks against provided data

        current_data: dictionary { 'date1': value1, 'date2': value2...}
        next_data: dictionary with one record { 'date_not_in_current_month': valueN }

        dates - format as string YYYY-MM-DD
        """
        if not (isinstance(next_data, dict) or isinstance(current_data, dict)):
            raise Exception("Input data is expected as a dictionary of dates -> indexes.")

        if len(current_data) < 1:
            raise Exception("Current data should contain at least one record.")

        if len(next_data) != 1:
            raise Exception("Next data should be a dictionary with only one value.")

        # CHECK1 if the current_data keys are dates
        check_same_month = {}
        for key in current_data.keys():
            try:
                date_obj = datetime.strptime(key, '%Y-%m-%d')
                check_same_month[str(date_obj.month) + str(date_obj.year)] = 'ok'
            except ValueError:
                raise Exception("The keys of the input data should be dates in the YYYY-MM-DD format.")

        # CHECK2 if the current_data dates are in the same month
        if len(check_same_month) != 1:
            # two or more keys means that we don't have the same month anymore
            raise Exception("The provided current dates are not in the same month.")
        else:
            # we grab (any) previously saved date_obj and calculate some useful dates
            last_day = calendar.monthrange(date_obj.year, date_obj.month)[1]
            self.__last_day_current_month = date_obj.replace(day=last_day).date()

            dh = DateHelper(as_object=True)
            self.__first_day_next_month = dh.get_first_day_next_month(date_obj)

        # CHECK3 if the month in the next_data is different then the current month
        try:
            next_data_item = next_data.copy().popitem()[0]
            next_obj = datetime.strptime(next_data_item, '%Y-%m-%d')
            next_stamp = str(next_obj.month) + str(next_obj.year)
        except ValueError:
            raise Exception("The keys of the input data should be dates in the YYYY-MM-DD format.")

        if next_stamp in check_same_month.keys():
            raise Exception("The next data should contain a date in a different month then current.")

        self.current_data = current_data
        self.next_data = next_data

    def estimate(self, estimation_day = None):
        """
        main estimation function using different methods

        :param estimation_day - day as number (e.g. 12, 30)
                by default: last day of the month
        :returns [[value1, value2], [estimation_date, next_date]]
        """
        edate, ndate = self.__calculate_estimation_dates(estimation_day)

        last_date, last_value = self.next_data.copy().popitem()

        # first we try the 'smart' estimation method but only if we have at least 2 records
        # otherwise, skip directly to second method
        apply_median_method = (len(self.current_data) < 2)
        if not apply_median_method:
            estimated_values = self.estimate_numpy_method(self.current_data, edate.day)

            # now check if estimation if not bigger than the provided limit
            # if it is, continue with the second estimation method
            apply_median_method = (estimated_values[0] >= last_value)

        # second method
        if apply_median_method:
            estimated_values = self.estimate_median_method(self.current_data, edate.day, last_date, last_value)

        # one last check: if next_date is the first day of the next month, we return that value instead
        # of estimation (no need for it for estimation when we already have it, right?)
        if ndate.isoformat() == last_date:
            estimated_values[1] = last_value

        # results
        self.estimated_values = estimated_values
        self.estimated_dates = [edate, ndate]

        return [self.estimated_values, self.estimated_dates]

    def estimate_numpy_method(self, input_data, estimated_day):
        """
        estimation done using numpy (polynomial)
        """
        feed_data = self.__prepare_data(input_data)

        data = numpy.array(feed_data)
        fit = numpy.polyfit(data[:,0], data[:,1], 1) # 1 means linear fit
        line = numpy.poly1d(fit)

        # estimation done for two consecutive days, starting with estimated_day
        estimation = line(numpy.arange(2) + estimated_day)
        estimation = list(map(int, estimation))

        return estimation

    def estimate_median_method(self, input_data, estimated_day, next_date, next_value):
        """
        estimation done using simple median calculation
        """
        dates = list(input_data.keys())
        dates.sort()

        starting_date = datetime.strptime(dates[-1], "%Y-%m-%d")
        starting_value = input_data[dates[-1]]
        last_date = datetime.strptime(next_date, "%Y-%m-%d")

        total_days = (last_date - starting_date).days
        total_value = next_value - starting_value

        median = total_value / total_days

        elast = starting_value + (estimated_day - starting_date.day) * median
        enext = elast + median

        return [int(elast),int(enext)]

    def __prepare_data(self, raw_data):
        """
        prepares the data in a numpy format
        e.g. input: {'2015-02-05': 10, '2015-02-06': 12...}
            output: [5,10], [6,12]... where the x - extracted day y - value
        """
        b = []
        for day, value in raw_data.items():
            # extract only the day from the date, and make it integer
            b.append([int(day[-2:]), value])

        b.sort()

        return b

    def __calculate_estimation_dates(self, estimation_day):
        """
        calculates the dates based on estimation day (as integer)
        first date - the one containing estimation day as number (default: last in month)
        next date - the next day of the first date

        :return: first_date (date object), next_date (date object)
        """
        if estimation_day:
            first_date = self.__last_day_current_month.replace(day=estimation_day) # building the full date version
            next_date = first_date + timedelta(1)

            first_date = first_date
            next_date = next_date
        else:
            first_date = self.__last_day_current_month
            next_date = self.__first_day_next_month

        return first_date, next_date