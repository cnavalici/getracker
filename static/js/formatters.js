/* various formatters for dGrid columns */

define(["dojo/date/locale"], function(locale){
    return {
        /* date formatter to display */
        date_formatter: function(item) {
            formatted = locale.format(new Date(item), {
                selector: "date",
                formatLength: "long"
            });

            return formatted;
        },

        /* date formatter to display as month-year */
        date_formatter_simple: function(item) {
            formatted = locale.format(new Date(item), {
                selector: "date",
                datePattern: "MMMM y"
            });

            return formatted;
        },

        /* total formatter for costs - not sure if that's the proper approach, but it works fine for now */
        total_costs_formatter: function(item, obj) {
            var tc = parseFloat(obj.costs_fixed) + parseFloat(obj.costs_variable);
            return tc.toFixed(2);
        }
    }
})