require([
	'dojo/_base/declare',
	'dstore/RequestMemory',
	'dstore/Trackable',
	'dgrid/OnDemandGrid',
	'dgrid/extensions/ColumnHider',
	'dgrid/extensions/ColumnReorder',
	'dgrid/extensions/ColumnResizer',
	'dgrid/extensions/CompoundColumns',
	'dgrid/Selection',
	'dojo/i18n!i18n/nls/translations',
	'js/urls',
	'js/formatters',
	'js/summaryRow'
], function (declare, RequestMemory, Trackable, OnDemandGrid, ColumnHider, ColumnReorder, ColumnResizer,
            CompoundColumns, Selection, t, URLS, format, SummaryRow) {

    // Records store
	var storeRec = new (declare([RequestMemory, Trackable]))({
	    target: URLS.dashboardGetRecordedIndexes
	});

	// define a new grid type
	var baseGridType = declare([OnDemandGrid, ColumnHider, SummaryRow, Selection, ColumnResizer]);

	// Instantiate records grid
	var recordsGridType = declare([baseGridType, ColumnReorder]);
	var gridRecords = new recordsGridType({
		collection: storeRec,
		noDataMessage: t.no_results_found,
		columns: [
		    { field: 'id', hidden: true, unhidable: true },
            { field: 'date_recorded', label: t.date_recorded, formatter: format.date_formatter },
            { field: 'index_value', label: t.index_value },
            { field: 'energy_type', label: t.energy_type }
		]
	}, 'gridRecords');

	gridRecords.startup();
	window.gridRecords = gridRecords;

	// Calculated costs store
	var storeCalc = new (declare([RequestMemory, Trackable]))({
	    target: URLS.dashboardGetCalculatedCosts
	});

	storeCalc.getTotals = function() {
		var totals = {'date_recorded': 'Total', 'diff_index': 0, 'costs_variable': 0, 'costs_fixed': 0};

        this.forEach(function(item) {
            totals['diff_index'] += item['diff_index'];
            totals['costs_variable'] += parseFloat(item['costs_variable']);
            totals['costs_fixed'] += parseFloat(item['costs_fixed']);
            totals['costs_total'] = totals['costs_variable'] + totals['costs_fixed'];
        });

        totals['costs_variable'] = parseFloat(totals['costs_variable'].toFixed(2));
        totals['costs_fixed'] = parseFloat(totals['costs_fixed'].toFixed(2));
        totals['costs_total'] = parseFloat(totals['costs_total'].toFixed(2));

		return totals;
	}


	// Instantiate records grid
	var calculatedGridType = declare([baseGridType, CompoundColumns, ColumnReorder]);
	var gridCalculated = new calculatedGridType({
		collection: storeCalc,
		noDataMessage: t.no_results_found,
		columns: [
			{ field: 'id', hidden: true, unhidable: true },
			{ field: 'date_recorded', label: t.date_recorded, formatter: format.date_formatter_simple },
			{ label: t.indexes_header, children: [
			    { field: 'first_index', label: t.first_index },
			    { field: 'last_index', label: t.last_index },
			    { field: 'diff_index', label: t.diff_index }
			]},
			{ label: t.costs_header, children: [
			    { field: 'costs_variable', label: t.costs_variable },
			    { field: 'costs_fixed', label: t.costs_fixed },
			    { field: 'costs_total', label: t.costs_total, formatter: format.total_costs_formatter }
			]},
			{ field: 'energy_type', label: t.energy_type }
		]
	}, 'gridCalculated');

	gridCalculated.startup();
	gridCalculated.on('dgrid-refresh-complete', function(event) {
	    event.grid.set('summary', storeCalc.getTotals());
	});

	window.gridCalculated = gridCalculated;
});
