require(["dijit", "dojo/request", "dojo/cookie", "dojo/date/locale", "dojo", "dojo/html", "dojo/dom-attr", "js/urls",
        "js/generic"],
    function(dijit, request, cookie, locale, dojo, html, domAttr, urls, GT) {
    submitForm = function() {
        var form = dijit.byId("frm_new_indexes");

        if (!form.validate()) {
            return false;
        }

        // not so pretty way to convert the date, but it seems that dijit is making it unnecessarily complicated
        var formValues = form.get('value');
        // formValues.date_recorded = formValues.date_recorded.toISOString();
        formValues.date_recorded = locale.format(formValues.date_recorded, {
            selector: 'date', datePattern: 'yyyy-MM-dd'
        });

        request.post(urls.dashboardSaveIndex,
            {
                data:formValues,
                headers:{'X-CSRFToken': cookie('csrftoken')}
            }
        ).then(
            function(response) {
                var r = JSON.parse(response);

                if (r.result == 0) {
                    // this is the ERROR msg
                    domAttr.set("form_message", "style", { "color": "red"});
                }
                html.set(dojo.byId("form_message"), r.msg);

                setTimeout(function() {
                    html.set(dojo.byId("form_message"), '&nbsp;');
                }, 3000);

                GT.refreshGrid(window.gridRecords);
                GT.refreshGrid(window.gridCalculated);
            }
        );
    }
});