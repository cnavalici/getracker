define([], function(){
    return {
        /* generic function to refresh a grid */
        refreshGrid: function(targetGrid) {
            var store = targetGrid.collection;
            store.invalidate()
            store.fetch()
            targetGrid.refresh()
        }
    }
});