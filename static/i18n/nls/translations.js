var en_trans = {
    index_value: "Index Value",
    date_recorded: "Date Recorded",
    energy_type: "Energy Type",
    no_results_found: "No results found.",
    indexes_header: "Indexes",
    first_index: "First",
    last_index: "Last",
    diff_index: "Difference",
    costs_header: "Costs",
    costs_variable: "Variable",
    costs_fixed: "Fixed",
    costs_total: "Total",
};

define({
    root: en_trans,
    'ro': false
});