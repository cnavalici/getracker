# GETracker

[![Coverage Status](https://coveralls.io/repos/bitbucket/cnavalici/getracker/badge.svg?branch=master)](https://coveralls.io/bitbucket/cnavalici/getracker?branch=master)

## What is GETracker?
GETracker is a hobby project built on top of Django Framework and Dojo Toolkit. It allows a simple tracking of the gas and electricity consumption based on the provided indexes for a home user. The tariffs are taken from energiedirect.nl (Netherlands market specific).

## Current status

Work in progress towards a beta version.

### Version
Soon to follow.

### Installation

Soon to follow.

### Development

Want to contribute? Great!
Soon to follow.
