from django.template import RequestContext
from django.shortcuts import render_to_response, redirect
# from django.http import HttpResponseRedirect
# from django.core.urlresolvers import reverse
from django.contrib import messages
from django.utils.translation import ugettext as _
from django.conf import settings

from .forms import LoginForm, AccountForm
from .models import Accounts
from lib.accounts import AccountsManager


def login(request):
    """Also landing page for accountsmgm part"""
    # pre-populate the form if we already have a code in the session
    saved_code = request.session.get(settings.SESSION_ACCOUNT_CODE, False)
    if saved_code:
        login_form = LoginForm({'account_code': saved_code})
    else:
        login_form = LoginForm()

    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            account = Accounts.objects.filter(unique_code=form.cleaned_data['account_code']).first()
            if account:
                request.session[settings.SESSION_ACCOUNT_ID] = account.id
                request.session[settings.SESSION_ACCOUNT_CODE] = account.unique_code
                return redirect('dashboard:main_page') #return HttpResponseRedirect(reverse('dashboard:main_page'))
            else:
                messages.error(request, _("It seems that your provided code cannot be found."))

    template_data = {
        'form': login_form,
        'activeMenu': 'login'
    }

    return render_to_response('accountsmgm/login.html', template_data, context_instance=RequestContext(request))


def recover(request):
    """Recovering all the accounts codes related to an email address"""
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            manager = AccountsManager()
            manager.recover_account(form.cleaned_data['account_email'])

            messages.success(request, _("An email should arrive shortly to the provided email address."))

            return redirect('accountsmgm:recover_page')

    template_data = {
        'form': AccountForm,
        'activeMenu': 'recover'
    }

    return render_to_response('accountsmgm/recover.html', template_data, context_instance=RequestContext(request))


def new_account(request):
    """Creating a new token (aka new account)"""
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            manager = AccountsManager()
            new_code = manager.create_new_account(form.cleaned_data['account_email'])

            # save it to session for fast login
            request.session[settings.SESSION_ACCOUNT_CODE] = new_code

            return redirect('login_page')

    template_data = {
        'form': AccountForm,
        'activeMenu': 'getnew'
    }

    return render_to_response('accountsmgm/new.html', template_data, context_instance=RequestContext(request))

