"""
python manage.py makemigrations --dry-run accountsmgm
python manage.py sqlmigrate accountsmgm <name_of_the_migration_file>
python manage.py migrate
"""

from django.db import models
from django.utils import timezone


class Accounts(models.Model):
    unique_code = models.CharField(max_length=36, db_index=True)
    email_address = models.CharField(max_length=120)
    created_at = models.DateTimeField(auto_now=True)
    accessed_at = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.unique_code
