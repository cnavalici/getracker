# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accountsmgm', '0002_auto_20151030_1349'),
    ]

    operations = [
        migrations.AlterField(
            model_name='accounts',
            name='unique_code',
            field=models.CharField(max_length=36, db_index=True),
        ),
    ]
