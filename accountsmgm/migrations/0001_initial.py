# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Accounts',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('unique_code', models.CharField(max_length=36)),
                ('email_address', models.CharField(max_length=120)),
                ('created_at', models.DateTimeField(auto_now=True)),
                ('accessed_at', models.DateTimeField(default=datetime.datetime.now)),
            ],
        ),
    ]
