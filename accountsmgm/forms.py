from django import forms
from django.utils.translation import ugettext as _

from lib.validators import validate_uuid4

REGEX_UUID4 = '[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}'


class LoginForm(forms.Form):
    """ pseudo login form - it only takes one code as credentials """
    account_code = forms.CharField(
        label=_("Account Code"), required=True, max_length=36,
        widget=forms.TextInput(attrs={
            'trim': 'trim',
            'required': 'required',
            'style': "width: 300px;",
            'data-dojo-type': 'dijit/form/ValidationTextBox',
            'data-dojo-props': "invalidMessage: '%s', pattern: '%s'" % (_("A valid Account ID is expected"), REGEX_UUID4),
            'placeholder': 'e.g.: fe41d31e-c78c-446c-85e0-bd67db934860'
        }),
        validators=[validate_uuid4]
    )


class AccountForm(forms.Form):
    """ new accounts form, used also for recovering codes """
    account_email = forms.EmailField(
        label=_("Account Email"), required=True, max_length=36,
        widget=forms.TextInput(attrs={
            'trim': 'trim',
            'required': 'required',
            'style': "width: 300px;",
            'placeholder': 'john.doe@example.com',
            'data-dojo-type': 'dijit/form/ValidationTextBox',
            'validator': 'dojox.validate.isEmailAddress',
            'invalidMessage': _("A valid email address is required")
        })
    )