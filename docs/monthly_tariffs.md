# Monthly tariffs

## DB information structure

The montly tariffs are saved into db with different names and a time range.

_Example_:

    ID:1 Vaste leveringkosten    2015-01-01 to 2015-04-30
    ID:2 Netbeheerkosten         2015-01-01 to 2015-04-30

But also we might have some tariffs that don't overlap exactly the previous dates:

    ID:3 Netbeheerkosten 2       2015-03-01 to 2015-06-30

## Expected functionality

The monthly tariffs module should be able to give a sum of all taxes for any given point in time. The expected result looks like:

    2015-01-01 to 2015-02-28 Vaste leveringkosten + Netbeheerkosten
    2015-03-01 to 2015-04-30 Vaste leveringkosten + Netbeheerkosten + Netbeheerkosten 2
    2015-05-01 to 2015-06-30 Netbeheerkosten 2 only

And more _graphical_:

    2015-01-01      2015-03-01           2015-04-30   2015-06-30
    |-------- ID1 ------|---------------------|
    |-------- ID2 ------|---------------------|
                        |-------- ID3 --------------------|

## Code logic

After retrieving all the records for a specific energy_type from the DB:

1) first step is to extract all the dates (start and end) and combine them in an ordered list (milestones)
2) iterate over the milestones list: for every new milestone consider a new interval (start - end date) and get the associated tariffs for that period - create the sum of them
3) take into consideration that the end date should be one day less than the next start date, unless it's already at this value


## Last update:
09.12.2015      Cristian Năvălici