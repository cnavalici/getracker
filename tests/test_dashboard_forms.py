"""
    coverage run --source='dashboard/' manage.py test -v 2
    coverage report -m
"""
import random

from django.test import TestCase
from dashboard.forms import IndexesForm
from dashboard.models import ELECTRICITY_CODE, GAS_CODE
from datetime import datetime


class IndexesFormsTest(TestCase):
    def test_indexes_form_invalid_date(self):
        input_data = {'index_value': self.get_valid_index(), 'energy_type': ELECTRICITY_CODE}

        invalid_dates = ('not_valid_date', '2011-01-01', '2099-08-12', '2055-55-55')

        for i_date in invalid_dates:
            input_data['date_recorded'] = i_date

            form = IndexesForm(data=input_data)
            self.assertFalse(form.is_valid())

    def test_indexes_form_invalid_energy_type(self):
        input_data = {'index_value': 1, 'date_recorded': self.get_valid_date()}

        input_data['energy_type'] = 999

        form = IndexesForm(data=input_data)
        self.assertFalse(form.is_valid())

    def test_indexes_form_invalid_index(self):
        input_data = {'energy_type': GAS_CODE, 'date_recorded': self.get_valid_date()}

        invalid_indexes = ('not_valid_index', 0, -1, self.get_valid_index(False))

        for i_index in invalid_indexes:
            input_data['index_value'] = i_index

            form = IndexesForm(data=input_data)
            self.assertFalse(form.is_valid())

    def test_indexes_form_valid(self):
        input_data = {'index_value': self.get_valid_index(), 'date_recorded': self.get_valid_date(),'energy_type': GAS_CODE}
        form = IndexesForm(data=input_data)
        self.assertTrue(form.is_valid())

    def get_valid_date(self):
        """
        helper function used to provide a valid date between a range of accepted dates
        """
        form = IndexesForm()
        today = int(form.TODAY_DATE.timestamp())
        max_date = int(form.MAX_DATE.timestamp())

        random_timestamp = random.randint(today, max_date)
        random_date = datetime.utcfromtimestamp(random_timestamp).date()

        return random_date

    def get_valid_index(self, valid=True):
        """
        helper used to generate a valid/invalid index
        flag valid=False will provide an invalid value
        """
        form = IndexesForm()
        random_index = random.randint(form.INDEX_MIN, form.INDEX_MAX)

        if not valid:
            random_index = random_index + form.INDEX_MAX # make it invalid

        return random_index