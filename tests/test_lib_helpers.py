"""
    coverage run --source='lib/' manage.py test -v 2
    coverage report -m

    python manage.py test tests.test_lib_helpers
"""

from django.test import TestCase
from lib.helpers import DateHelper
from datetime import datetime

class DateHelperLibTest(TestCase):
    def setUp(self):
        self.dh_1 = DateHelper(as_object=False)
        self.dh_2 = DateHelper(as_object=True)

    def test_get_first_day_next_month_same_year(self):
        current_date = datetime.strptime('2015-03-05', DateHelper.date_format)

        r1 = self.dh_1.get_first_day_next_month(current_date)
        self.assertEqual(r1, '2015-04-01')

        r2 = self.dh_2.get_first_day_next_month(current_date)
        self.assertEqual(r2.strftime('%Y-%m-%d'), '2015-04-01')

    def test_get_first_day_next_month_different_year(self):
        current_date = datetime.strptime('2015-12-05', DateHelper.date_format)

        r1 = self.dh_1.get_first_day_next_month(current_date)
        self.assertEqual(r1, '2016-01-01')

        r2 = self.dh_2.get_first_day_next_month(current_date)
        self.assertEqual(r2.strftime('%Y-%m-%d'), '2016-01-01')

    def test_difference_in_months_same_date(self):
        d1 = d2 = datetime.strptime('2015-01-01', '%Y-%m-%d').date()
        diff_1 = DateHelper.difference_in_months(d1, d2)

        self.assertEqual(diff_1, 0)

    def test_difference_in_months_same_year(self):
        d1 = datetime.strptime('2015-01-01', '%Y-%m-%d').date()
        d2 = datetime.strptime('2015-12-01', '%Y-%m-%d').date()

        diff_1 = DateHelper.difference_in_months(d1, d2)
        diff_2 = DateHelper.difference_in_months(d2, d1)

        self.assertEqual(diff_1, 11)
        self.assertEqual(diff_2, 11)

    def test_difference_in_months_different_years(self):
        d1 = datetime.strptime('2015-01-01', '%Y-%m-%d').date()
        d2 = datetime.strptime('2016-05-11', '%Y-%m-%d').date()
        d3 = datetime.strptime('2018-12-14', '%Y-%m-%d').date()
        d4 = datetime.strptime('2016-01-14', '%Y-%m-%d').date()

        diff_1 = DateHelper.difference_in_months(d1, d2)
        diff_2 = DateHelper.difference_in_months(d3, d1)
        diff_3 = DateHelper.difference_in_months(d2, d3)
        diff_4 = DateHelper.difference_in_months(d4, d1)

        self.assertEqual(diff_1, 16)
        self.assertEqual(diff_2, 47)
        self.assertEqual(diff_3, 31)
        self.assertEqual(diff_4, 12)