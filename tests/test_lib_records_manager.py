"""
    coverage run --source='lib/' manage.py test -v 2
    coverage report -m

    use python manage.py dumpdata <module>.<modelName> to extract some existing data

    python manage.py test tests.test_lib_records_manager
"""

from django.test import TestCase

from datetime import datetime
from dateutil.relativedelta import relativedelta

from lib.records import RecordsManager
from lib.estimator import IndexEstimator
from lib.costs import CostsManager

from dashboard.models import ELECTRICITY_CODE, GAS_CODE, CalculatedCosts, Records


class RecordsManagerLibTest(TestCase):
    fixtures = ['accounts.json', 'records.json']

    current_account = 1
    other_account = 2

    def setUp(self):
        self.rm_electricity = RecordsManager(energy_type=ELECTRICITY_CODE, account_id=self.current_account)
        self.rm_gas = RecordsManager(energy_type=GAS_CODE, account_id=self.current_account)

        self.rm_electricity.costs_manager = CostsManager
        self.rm_gas.costs_manager = CostsManager

        self.rm_electricity.index_estimator = IndexEstimator
        self.rm_gas.index_estimator = IndexEstimator

        CalculatedCosts.objects.all().delete()

    def test_initializing(self):
        self.assertEqual(self.rm_electricity.energy_type, ELECTRICITY_CODE)

    def test_initializing_required_account_id(self):
        self.assertRaises(Exception, RecordsManager, {}, {'energy_type':ELECTRICITY_CODE, 'account_id':None})

    def test_get_daily_record_electricity(self):
        # for electricity - existing record (fixture)
        electric = self.rm_electricity.get_daily_record('2015-11-11')
        self.assertEqual(electric.pk, 1)

        # for electricity - non-existing record
        electric = self.rm_electricity.get_daily_record('2014-01-01')
        self.assertIsNone(electric)

    def test_get_daily_record_gas(self):
        # for gas - existing record (fixture)
        gas = self.rm_gas.get_daily_record('2015-11-11')
        self.assertEqual(gas.pk, 2)

        # for gas - non-existing record
        gas = self.rm_gas.get_daily_record('2014-01-01')
        self.assertIsNone(gas)

    def test_get_all_records_no_mapper(self):
        all = self.rm_electricity.get_all_records()
        self.assertIsNotNone(all)

        # check if we have reverse order here
        first = all.first()
        self.assertEqual(first.date_recorded.__str__(), '2015-12-05')

    def test_get_all_records_with_mapper(self):
        # simple mapper assigned
        mapper = lambda data: len(data)
        all = self.rm_electricity.get_all_records(mapper)

        self.assertEqual(all, 6)  # number of records in the fixture

    def test_get_open_month_no_records(self):
        norecord = self.rm_electricity.get_open_month()
        self.assertEqual(norecord, -1)

    def test_get_open_month_multiple_records(self):
        CalculatedCosts.objects.bulk_create([
            CalculatedCosts(account_id=self.current_account, date_recorded='2015-01-01', first_index=1000,
                            last_index=1200, costs_variable=0, costs_fixed=0, energy_type=ELECTRICITY_CODE),
            CalculatedCosts(account_id=self.current_account, date_recorded='2015-02-01', first_index=1201,
                            last_index=0, costs_variable=0, costs_fixed=0, energy_type=ELECTRICITY_CODE)
        ])

        open_month_date = self.rm_electricity.get_open_month().date_recorded.__str__()
        self.assertEqual(open_month_date, '2015-02-01')

        CalculatedCosts.objects.all().delete()
        CalculatedCosts.objects.bulk_create([
            CalculatedCosts(account_id=self.current_account, date_recorded='2015-02-01', first_index=1400,
                            last_index=1450, costs_variable=0, costs_fixed=0, energy_type=ELECTRICITY_CODE),
            CalculatedCosts(account_id=self.current_account, date_recorded='2015-03-01', first_index=1451,
                            last_index=0, costs_variable=0, costs_fixed=0, energy_type=ELECTRICITY_CODE),
            CalculatedCosts(account_id=self.current_account, date_recorded='2015-02-01', first_index=500,
                            last_index=0, costs_variable=0, costs_fixed=0, energy_type=GAS_CODE)
        ])

        open_month_el = self.rm_electricity.get_open_month().date_recorded.__str__()
        self.assertEqual(open_month_el, '2015-03-01')

        open_month_gas = self.rm_gas.get_open_month().date_recorded.__str__()
        self.assertEqual(open_month_gas, '2015-02-01')

    def test_get_open_month_records(self):
        target_date = datetime.strptime('2014-12-01', '%Y-%m-%d').date()
        open_month_records_no_mapper = self.rm_electricity.get_open_month_records(target_date)

        self.assertEqual(len(open_month_records_no_mapper), 2)
        self.assertIsInstance(open_month_records_no_mapper[0], Records)

        mapper = lambda x: x
        open_month_records_with_mapper = self.rm_electricity.get_open_month_records(target_date, mapper)

        self.assertEqual(len(open_month_records_with_mapper), 2)
        self.assertIsInstance(open_month_records_no_mapper[0], Records)

    def test_do_previous_months_calculation_no_initial_data(self):
        input_data = {'energy_type': ELECTRICITY_CODE, 'index_value': 1,
                      'date_recorded': datetime.strptime('2014-12-01', '%Y-%m-%d').date()}

        self.rm_electricity.do_previous_months_calculation(input_data)

        # now we should have on record in precalculated costs
        n = CalculatedCosts.filtered.account(self.current_account).all()
        nr = n.first()
        self.assertEqual(len(n), 1)
        self.assertEqual(nr.date_recorded, input_data['date_recorded'])
        self.assertEqual(nr.last_index, 0)

    def test_do_previous_months_calculation_one_record_not_open(self):
        cc = CalculatedCosts(account_id=self.current_account, date_recorded='2011-01-01', first_index=1000,
                             last_index=1200, costs_variable=0, costs_fixed=0, energy_type=ELECTRICITY_CODE)
        cc.save()

        input_data = {'energy_type': ELECTRICITY_CODE, 'index_value': 1,
                      'date_recorded': datetime.strptime('2014-12-01', '%Y-%m-%d').date()}

        # no open months is unacceptable so an Exception will be raised
        self.assertRaises(Exception, self.rm_electricity.do_previous_months_calculation, input_data, {})

    def test_do_previous_months_calculation_the_same_open_month(self):
        # insert one open month for different accounts
        CalculatedCosts.objects.bulk_create([
            CalculatedCosts(account_id=self.current_account, date_recorded=datetime.utcnow().date(), first_index=1000,
                            last_index=0, costs_variable=0, costs_fixed=0, energy_type=ELECTRICITY_CODE),
            CalculatedCosts(account_id=self.other_account, date_recorded=datetime.utcnow().date(), first_index=1201,
                            last_index=0, costs_variable=0, costs_fixed=0, energy_type=ELECTRICITY_CODE)
        ])

        input_data = {'energy_type': ELECTRICITY_CODE, 'index_value': 1,
                      'date_recorded': datetime.utcnow().date()}

        r = self.rm_electricity.do_previous_months_calculation(input_data)

        self.assertEqual(r, 0)

    def test_do_previous_months_calculation_final_calculation_one_month(self):
        one_month_ago = datetime.utcnow().date() + relativedelta(months=-1)

        Records.objects.all().delete()
        r = Records(account_id=self.current_account, index_value=1200, date_recorded=one_month_ago,
                    energy_type=GAS_CODE)
        r.save()

        cc = CalculatedCosts(account_id=self.current_account, date_recorded=one_month_ago, first_index=1000,
                             last_index=0, costs_variable=0, costs_fixed=0, energy_type=GAS_CODE)
        cc.save()

        input_data = {'energy_type': GAS_CODE, 'index_value': 2000, 'date_recorded': datetime.utcnow().date()}

        r1 = self.rm_gas.do_previous_months_calculation(input_data)
        self.assertEqual(r1, 1)

        n1 = CalculatedCosts.filtered.account(self.current_account).all()
        self.assertEqual(n1[0].last_index, 0)

    def test_do_previous_months_calculation_final_calculation_three_months(self):
        three_months_ago = datetime.utcnow().date() + relativedelta(months=-3)

        Records.objects.all().delete()
        r = Records(account_id=self.current_account, index_value=1200, date_recorded=three_months_ago,
                    energy_type=ELECTRICITY_CODE)
        r.save()

        cc = CalculatedCosts(account_id=self.current_account, date_recorded=three_months_ago, first_index=1000,
                             last_index=0, costs_variable=0, costs_fixed=0, energy_type=ELECTRICITY_CODE)
        cc.save()

        input_data = {'energy_type': ELECTRICITY_CODE, 'index_value': 5000, 'date_recorded': datetime.utcnow().date()}

        r1 = self.rm_electricity.do_previous_months_calculation(input_data)
        self.assertEqual(r1, 3)

        n1 = CalculatedCosts.filtered.account(self.current_account).all()
        self.assertEqual(n1[0].last_index, 0)
