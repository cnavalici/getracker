"""
    coverage run --source='lib/' manage.py test -v 2
    coverage report -m

    python manage.py test tests.test_lib_estimator
"""

from django.test import TestCase
from lib.estimator import IndexEstimator


class IndexEstimatorLibTest(TestCase):
    def setUp(self):
        self.ie = IndexEstimator()

    def test_populate_with_wrong_input_format(self):
        self.assertRaises(Exception, self.ie.populate, 123, 123)

    def test_populate_with_not_enough_data(self):
        # bad current data, bad last data
        self.assertRaises(Exception, self.ie.populate, {}, {})

        # good current data, bad last data
        self.assertRaises(Exception, self.ie.populate, {'2013-01-01': 123, '2013-02-02': 145},
                          {'2013-01-01': 123, '2013-02-02': 145})

        # bad current_data, good last data
        self.assertRaises(Exception, self.ie.populate, {}, {'2013-01-01': 123})
        self.assertRaises(Exception, self.ie.populate, {'2013-01-01': 123}, {'2013-01-01': 123})

    def test_populate_with_wrong_format_input(self):
        current_data = {'2015-11-20': 12, '2015-11-22': 15}
        last_data = {'20-12-01': 19}  # wrong format here

        self.assertRaises(Exception, self.ie.populate, current_data, last_data)

        current_data = {'2015-11-20': 12, '2019/11/22': 15}  # wrong format here
        last_data = {'2014-12-01': 19}

        self.assertRaises(Exception, self.ie.populate, current_data, last_data)

    def test_populate_with_current_dates_different_month(self):
        current_data = {'2015-11-20': 12, '2015-11-22': 15, '2015-12-05': 18}
        last_data = {'2016-01-01': 19}

        self.assertRaises(Exception, self.ie.populate, current_data, last_data)

    def test_populate_with_last_date_same_current_month(self):
        current_data = {'2015-11-20': 12, '2015-11-22': 15, '2015-11-23': 18}
        last_data = {'2015-11-25': 19}  # same month, 11th

        self.assertRaises(Exception, self.ie.populate, current_data, last_data)

    def test_populate_with_good_behaving_data(self):
        current_data = {'2015-11-20': 12, '2015-11-22': 15, '2015-11-23': 18}
        last_data = {'2016-11-25': 219}  # same month, 11th but different year is ok

        self.ie.populate(current_data, last_data)
        self.assertEqual(self.ie.current_data, current_data)
        self.assertEqual(self.ie.next_data, last_data)

    def test_numpy_method(self):
        input_data = {'2015-11-04': 10, '2015-11-05': 12, '2015-11-06': 14, '2015-11-07': 16}
        ed = 10
        result = self.ie.estimate_numpy_method(input_data, ed)

        self.assertEqual(result, [22, 24])

    def test_median_method(self):
        input_data = {'2015-11-20': 10, '2015-11-27': 20}
        next_date = '2015-12-02'
        next_value = 35
        ed = 30

        # we have 5 days as difference (27...02), value diff = 15 (35 - 20), median = 3
        result = self.ie.estimate_median_method(input_data, ed, next_date, next_value)

        self.assertEqual(result, [29, 32])

    def test_estimate_default_last_day_numpy_method(self):
        current_data = {'2015-11-20': 12, '2015-11-22': 15, '2015-11-23': 18}
        next_data = {'2015-12-02': 100}

        self.ie.populate(current_data, next_data)
        values, dates = self.ie.estimate()

        self.assertEqual(values, [31, 33])
        self.assertEqual(self.__transform(dates), ['2015-11-30', '2015-12-01'])

    def test_estimate_default_last_day_median_method(self):
        current_data = {'2015-11-20': 12, '2015-11-22': 15, '2015-11-23': 21}
        next_data = {'2015-12-02': 30} # numpy method gives 31 for this

        self.ie.populate(current_data, next_data)
        values, dates = self.ie.estimate()

        self.assertEqual(values, [28, 29])
        self.assertEqual(self.__transform(dates), ['2015-11-30', '2015-12-01'])

    def test_estimate_next_day_as_first(self):
        current_data = {'2015-11-28': 12, '2015-11-29': 15}
        next_data = {'2015-12-01': 18} # this value should be instead of estimation

        self.ie.populate(current_data, next_data)
        values, dates = self.ie.estimate()

        self.assertEqual(values, [17, 18])
        self.assertEqual(self.__transform(dates), ['2015-11-30', '2015-12-01'])

    def test_estimate_only_start_end_dates(self):
        # 122 days difference, median = 3.27, for 2015-12-31 we have 100 + 98 = 198
        current_data = {'2015-12-01': 100}
        next_data = {'2016-04-01': 500}

        self.ie.populate(current_data, next_data)
        values, dates = self.ie.estimate()

        self.assertEqual(values, [198, 201])
        self.assertEqual(self.__transform(dates), ['2015-12-31', '2016-01-01'])

    def test_estimate_with_specific_day(self):
        current_data = {'2015-11-15': 10, '2015-11-17': 14, '2015-11-19': 18}
        next_data = {'2015-12-01': 299}

        self.ie.populate(current_data, next_data)
        values, dates = self.ie.estimate(21)

        self.assertEqual(values, [22, 24])
        self.assertEqual(self.__transform(dates), ['2015-11-21', '2015-11-22'])

    def __transform(self, raw_result):
        """
        helper function to get dates as strings only
        """
        d0 = raw_result[0].isoformat()
        d1 = raw_result[1].isoformat()
        return [d0, d1]