"""
    coverage run --source='accountsmgm/' manage.py test -v 2
    coverage report -m
"""
import uuid

from django.test import TestCase
from accountsmgm.forms import LoginForm, AccountForm


class AccountsFormsTest(TestCase):
    def test_login_form_invalid(self):
        form = LoginForm({'account_code': ''})
        self.assertFalse(form.is_valid())

        form = LoginForm({'account_code': 'invalid_uuid4'})
        self.assertFalse(form.is_valid())

    def test_login_form_valid(self):
        uuid4 = str(uuid.uuid4())

        form = LoginForm({'account_code': uuid4})
        self.assertTrue(form.is_valid())

    def test_new_account_form_invalid(self):
        form = AccountForm({'account_email': ''})
        self.assertFalse(form.is_valid())

        form = AccountForm({'account_email': 'something_not_quite_right'})
        self.assertFalse(form.is_valid())

    def test_new_account_form_valid(self):
        form = AccountForm({'account_email': 'john.doe@example.com'})
        self.assertTrue(form.is_valid())
