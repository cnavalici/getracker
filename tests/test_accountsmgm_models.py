"""
    coverage run --source='accountsmgm/' manage.py test -v 2
    coverage report -m
"""

from django.test import TestCase
from accountsmgm.models import Accounts

class AccountsTest(TestCase):
    def create_account(self, unique_code='', email_address='default@example.com'):
        return Accounts.objects.create(unique_code=unique_code, email_address=email_address)

    def test_account_creation(self):
        account = self.create_account()
        self.assertIsInstance(account, Accounts)
        self.assertEqual(account.__str__(), account.unique_code)
