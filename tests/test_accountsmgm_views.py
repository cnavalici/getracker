"""
    coverage run --source='accountsmgm/' manage.py test -v 2
    coverage report -m
"""
from django.test import TestCase
from django.core.urlresolvers import reverse


class AccountsViewsTest(TestCase):
    def test_login_no_cookie_code(self):
        login_url = reverse("accountsmgm:login")
        response = self.client.get(login_url)

        self.assertEqual(response.status_code, 200)
        # print(response.context['form'])

