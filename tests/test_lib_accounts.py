"""
    coverage run --source='lib/' manage.py test -v 2
    coverage report -m
"""

from django.test import TestCase
from lib.accounts import AccountsManager
from accountsmgm.models import Accounts
from uuid import UUID

class AccountsLibTest(TestCase):
    def test_account_creation(self):
        target_email = 'johnny@example.com'

        am = AccountsManager()
        result_code = am.create_new_account(target_email)

        self.assertEqual(len(result_code), 36)
        self.assertTrue(UUID(result_code, version=4))

        dbobject = Accounts.objects.filter(email_address=target_email).first()
        self.assertEqual(dbobject.unique_code, result_code)


