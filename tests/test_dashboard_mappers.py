"""
    coverage run --source='dashboard.mappers' manage.py test -v 2
    coverage report -m
    coverage html

    python manage.py test tests.test_dashboard_mappers
"""

from django.test import TestCase
from django.forms.models import model_to_dict
from dashboard.mappers import grid_records_mapper, grid_calculated_mapper, fixed_costs_mapper
from dashboard.models import Records, ENERGY_TYPES, CalculatedCosts, TariffsMonthly


class MappersTest(TestCase):
    fixtures = ['accounts.json', 'records.json', 'calculated_costs.json']

    def test_grid_records_no_input_data(self):
        self.assertEqual(grid_records_mapper({}), [])

    def test_grid_records_with_data(self):
        input_data = Records.objects.all().order_by('-date_recorded')

        parsed = grid_records_mapper(input_data)[0]
        expected_keys = model_to_dict(input_data.first()).keys()

        self.assertEqual(parsed.keys(), expected_keys)

        energy_types = dict(ENERGY_TYPES)
        self.assertIs(parsed['energy_type'], energy_types[1])

    def test_grid_calculated_no_input_data(self):
        self.assertEqual(grid_calculated_mapper({}), [])

    def test_grid_calculated_with_data(self):
        input_data = CalculatedCosts.objects.all().exclude(last_index=0)

        parsed = grid_calculated_mapper(input_data)[0]
        exp_model = model_to_dict(input_data.first())
        exp_model['diff_index'] = 1234  # extra key with dummy value
        expected_keys = exp_model.keys()

        self.assertEqual(parsed.keys(), expected_keys)

        energy_types = dict(ENERGY_TYPES)
        self.assertIs(parsed['energy_type'], energy_types[1])

    def test_fixed_costs_with_data(self):
        input_data = TariffsMonthly.objects.all()
        e, g, t = fixed_costs_mapper(input_data)

        res_e = list(e[0].keys())
        res_e.sort()

        res_g = list(g[0].keys())
        res_g.sort()

        self.assertListEqual(['name', 'value'], res_e)
        self.assertListEqual(['name', 'value'], res_g)

        expected = ['bigtotal', 'electricity', 'gas']
        ordered_keys = list(t.keys())
        ordered_keys.sort()

        self.assertListEqual(expected, ordered_keys)
