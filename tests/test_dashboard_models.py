"""
    coverage run --source='dashboard/' manage.py test -v 2
    coverage report -m
"""

from django.test import TestCase
from django.utils import timezone
from dashboard.models import Records, TariffsMonthly, TariffsVariable, ELECTRICITY_CODE, GAS_CODE
from accountsmgm.models import Accounts


class DashboardModelsTest(TestCase):
    def create_account(self, unique_code='', email_address='default@example.com'):
        return Accounts.objects.create(unique_code=unique_code, email_address=email_address)

    def test_record_get_statuses(self):
        statuses = Records.get_statuses()
        self.assertEqual(len(statuses), 2)

    def test_record_creation(self):
        account = self.create_account()
        record = Records.objects.create(account=account, index_value=100, date_recorded=timezone.now(), energy_type=ELECTRICITY_CODE)

        statuses = Records.get_statuses()

        self.assertTrue(record.__str__(), statuses[ELECTRICITY_CODE])
        self.assertIsInstance(record, Records)

    def test_tariffs_monthly(self):
        tariff = TariffsMonthly(name="Dummy", value=123, energy_type=GAS_CODE,
                                start_date='2015-09-01', end_date='2015-09-22')
        self.assertIsInstance(tariff, TariffsMonthly)
        self.assertTrue('2015-09-01' in tariff.__str__())
        self.assertTrue('2015-09-22' in tariff.__str__())
        self.assertTrue('123' in tariff.__str__())
        self.assertTrue(str(GAS_CODE) in tariff.__str__())
        self.assertTrue('Dummy' in tariff.__str__())

    def test_tariffs_variable(self):
        tariff = TariffsVariable(value=123, energy_type=ELECTRICITY_CODE, start_date='2015-09-01', end_date='2015-09-22')
        self.assertIsInstance(tariff, TariffsVariable)
        self.assertTrue('2015-09-01' in tariff.__str__())
        self.assertTrue('2015-09-22' in tariff.__str__())
        self.assertTrue('123' in tariff.__str__())
        self.assertTrue(str(ELECTRICITY_CODE) in tariff.__str__())