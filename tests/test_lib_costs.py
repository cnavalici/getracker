"""
    coverage run --source='lib/' manage.py test -v 2
    coverage report -m

    python manage.py test tests.test_lib_costs
"""

from django.test import TestCase
from datetime import datetime

from lib.costs import CostsManager
from dashboard.models import ELECTRICITY_CODE, GAS_CODE, CalculatedCosts


class CostsManagerTest(TestCase):
    """
    based on costs.json (gas_code)
    2014-01-01 - 2015-06-30 5.00
    2015-07-01 - 2015-08-31 -28.25
    2015-09-01 - 2016-04-29 -24.25
    2016-04-30 - 2016-07-15 5.75
    2016-07-16 - 2016-12-31 2.00
    (electricity code)
    2015-07-01 - 2016-07-16 17.15
    2016-07-17 - 2016-12-31 13.40
    """
    fixtures = ['costs.json', 'calculated_costs.json', 'accounts.json']

    def setUp(self):
        self.cm_1 = CostsManager(energy_type=ELECTRICITY_CODE, account_id=1)
        self.cm_2 = CostsManager(energy_type=GAS_CODE)
        self.cm_3 = CostsManager()

    def test_check_monthly_tariffs_sums_gas(self):
        expected_data = {'2014-01-01': 5.00, '2014-05-01': 5.00, '2015-06-30': 5.00, '2015-07-04': -28.25,
                         '2015-10-11': -24.25, '2016-02-02': -24.25, '2016-05-01': 5.75, '2016-05-01': 5.75,
                         '2016-08-01': 2.00, '2016-12-31': 2.00, '2018-01-01': 0.00}
        for target_date, expected_sum in expected_data.items():
            td = datetime.strptime(target_date, '%Y-%m-%d').date()
            returned = self.cm_2.get_monthly_tariff(td)
            self.assertAlmostEqual(expected_sum, returned)

    def test_check_monthly_tariffs_sums_electricity(self):
        expected_data = {'2015-01-01': 0.00, '2016-01-01': 17.15, '2016-07-17': 13.40, '2016-12-01': 13.40,
                         '2018-01-01': 0.00}
        for target_date, expected_sum in expected_data.items():
            td = datetime.strptime(target_date, '%Y-%m-%d').date()
            returned = self.cm_1.get_monthly_tariff(td)
            self.assertAlmostEqual(expected_sum, returned)

    def test_check_variable_tariffs_gas(self):
        expected_data = {'2014-01-01': 6.69, '2015-06-30': 6.69, '2016-12-31': 6.69}
        for target_date, expected_tariff in expected_data.items():
            td = datetime.strptime(target_date, '%Y-%m-%d').date()
            returned = self.cm_2.get_variable_tariff(td)
            self.assertAlmostEqual(expected_tariff, returned)

    def test_check_variable_tariffs_electricity(self):
        expected_data = {'2014-01-01': 2.50, '2015-06-30': 2.50, '2015-09-05': 2.75, '2016-12-31': 2.75}
        for target_date, expected_tariff in expected_data.items():
            td = datetime.strptime(target_date, '%Y-%m-%d').date()
            returned = self.cm_1.get_variable_tariff(td)
            self.assertAlmostEqual(expected_tariff, returned)

    def test_get_all_calculated_costs_no_mapper(self):
        returned = self.cm_1.get_all_calculated_costs()

        self.assertEqual(1, len(returned))
        self.assertIsInstance(returned.first(), CalculatedCosts)

    def test_get_all_calculated_costs_with_mapper(self):
        mapper = lambda data: len(data)
        returned = self.cm_1.get_all_calculated_costs(mapper)

        self.assertEqual(1, returned)  # number of records in the fixture

    def test_get_all_fixed_costs_no_mapper_no_energy_type(self):
        # no energy type - records in costs.json fixture
        returned_no_energy_1 = self.cm_3.get_all_fixed_costs(target_date='2015-07-01')
        self.assertEqual(len(returned_no_energy_1), 5)

        returned_no_energy_2 = self.cm_3.get_all_fixed_costs(target_date='2016-12-31')
        self.assertEqual(len(returned_no_energy_2), 2)

    def test_get_all_fixed_costs_no_mapper_with_energy_type(self):
        # with energy type
        returned_gas = self.cm_2.get_all_fixed_costs(target_date='2015-07-01')
        self.assertEqual(len(returned_gas), 3)

        returned_electricity = self.cm_1.get_all_fixed_costs(target_date='2016-12-31')
        self.assertEqual(len(returned_electricity), 1)

    def test_get_all_fixed_costs_with_mapper(self):
        # apply a mapper over the results
        mapper = lambda data: "Size: {}".format(len(data))
        returned = self.cm_3.get_all_fixed_costs(mapper_func=mapper, target_date='2015-07-01')

        self.assertEqual("Size: 5", returned)

    def test_update_calculated_costs_no_record(self):
        non_existing_date = datetime.strptime('2000-01-01', '%Y-%m-%d')
        with self.assertRaises(Exception):
            self.cm_1.update_calculated_costs(month_end_date=non_existing_date, month_end_index=1000)

    def test_update_calculated_costs_existing_record(self):
        # the existing record coming from calculated_costs.json
        # we run a couple of small checks first (before update)
        existing_record = CalculatedCosts.objects.get(pk=2)
        self.assertEqual(0, existing_record.costs_variable)
        self.assertEqual(10000, existing_record.first_index)

        existing_date = datetime.strptime('2016-01-01', '%Y-%m-%d')
        self.cm_1.update_calculated_costs(month_end_date=existing_date, month_end_index=12000)

        # checks after update
        updated_record = CalculatedCosts.objects.get(pk=2)
        self.assertEqual(12000, updated_record.last_index)
        self.assertNotEqual(0, int(updated_record.costs_variable))
        self.assertNotEqual(0, int(updated_record.costs_fixed))


