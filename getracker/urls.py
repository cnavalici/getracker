"""getracker URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url, patterns

from accountsmgm import urls as accountsmgm_urls
from dashboard import urls as dashboard_urls
from accountsmgm import views as accountsmgm_views

urlpatterns = [
    url(r'^accountsmgm/', include(accountsmgm_urls, namespace='accountsmgm')),
    url(r'^dashboard/', include(dashboard_urls, namespace='dashboard')),
    url(r'^$', accountsmgm_views.login, name="login_page"),
]
