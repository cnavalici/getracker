"""
    python manage.py runserver --settings=getracker.conf.settings_local

    export DJANGO_SETTINGS_MODULE=getracker.conf.settings_local
"""

from .settings import *

# Database
# https://docs.djangoproject.com/en/dev/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'getracker',
        'USER': 'getracker',
        'PASSWORD': 'nn09h4589$uidfg90j',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}

INSTALLED_APPS += [
   'django_nose',
# 'debug_toolbar.apps.DebugToolbarConfig'
]

# Use nose to run all tests
TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'

# Tell nose to measure coverage
NOSE_ARGS = [
    '--with-coverage',
    '--cover-package=accountsmgm,getracker,lib',
    '--exclude-dir=/getracker/conf',
]

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

SECRET_KEY = '#znn33l+tq@^pu+lpc)s84(*)qv$^rr_gjlz@td6h(a0df3x@^'


# ============== EMAILS PART ===========================
# start a dumb server
# python -m smtpd -n -c DebuggingServer localhost:1025

EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025
EMAIL_DEFAULT_SENDER = 'no-reply@getracker.com'

# ============== ENDS EMAILS PART ======================

LOGGING['handlers']['file']['filename'] = os.path.join(BASE_DIR, 'logs/getracker.info.local.log')