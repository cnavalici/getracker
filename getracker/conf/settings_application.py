import os.path
from .settings import BASE_DIR

APP_VERSION = '0.2'
APP_NAME = 'GETracker'
APP_AUTHOR = 'Cristian Navalici'

# the names used for the session cookie
SESSION_ACCOUNT_ID = 'account_id'
SESSION_ACCOUNT_CODE = 'account_code'

GENERAL_LOG_NAME = 'getracker.general'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'file': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            # 'filename': this is filled in in the _production or _local settings files
            'formatter': 'simple'
        },
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(asctime)s -- %(message)s'
        },
    },
    'loggers': {
        GENERAL_LOG_NAME: {
            'handlers': ['file'],
            'level': 'INFO',
        },
    },
}

