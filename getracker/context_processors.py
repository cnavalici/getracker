from django.conf import settings

def global_settings(request):
    """ prefix every variable with GL_ """
    return {
        'GL_APP_VERSION': settings.APP_VERSION,
        'GL_APP_NAME': settings.APP_NAME,
        'GL_APP_AUTHOR': settings.APP_AUTHOR,
    }
